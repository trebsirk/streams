from random import choice

def find_majority(data_stream):
	majority = None
	counter = 0
	for item in data_stream:
		if item == majority:
			counter += 1
		else:
			if counter == 0:
				majority = item
				counter = 1
			else:
				counter -= 1
		print item, majority, counter
	return majority

def stream(items, maxi=2000):
	i = 0
	while True:
		yield choice(items)
		i += 1
		if i > maxi:
			break

l = range(10) + [5]*10
s = stream(l)
maj = find_majority(s)
print 'majority:', maj
