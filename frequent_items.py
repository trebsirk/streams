# algorithms for counting frequent items
# adapted from 
# Finding the Frequent Items in Streams of Data
# By Graham Cormode and Marios Hadjieleftheriou
# Communications of the ACM. October 2009. vol. 52. no. 10. pp. 97-105
from random import sample
from math import floor, sqrt

def frequent(data, k):
	n, T = 0, dict()
	for item in data:
		n = n + 1
		if T.get(item, 0) != 0:
			T[item] += 1
		elif len(T) <= k - 1:
			T[item] = 1
		else:
			for key in T.keys():
				T[key] -= 1
				if T[key] <= 0:
					del T[key]

	return T

def lossy_counting(data, k):
	n, delta, T = 0, 0, dict()
	for item in data:
		n = n + 1
		if T.get(item, 0) != 0:
			T[item] += 1
		else:
			T[item] = 1 + delta
	if n/k != delta:
		delta = n/k
		print 'delta:', delta
		for key in T.keys():
			if T[key] < delta:
				print key, T[key]
				del T[key]
	return T

def space_saving(data, k):
	n, T = 0, dict()
	for item in data:
		n += 1
		if T.get(item, 0) != 0:
			T[item] += 1
		elif len(T) < k:
			T[item] = 1
		else:
			min_item = min([(v,k) for k,v in T.items()])[1]
			T[item] = T[min_item]
			del T[min_item]
	return T

def is_prime(n):
	if n == 0 or n == 1:
		return True
	for x in range(2, int(sqrt(n))):
		if n % x == 0:
			return False
	return True

def universal_hash(x, a, b, p, N):
	return ((a*x + b) % p) % N

def plusminus1_hash(x, a, b, p, N):
	# split hash into two
	if (((a*x + b) % p) % N) < N/2:
		return 1
	else:
		return -1

def get_primes(a, b):
	primes = [x for x in range(a, b+1) if is_prime(x)]
	return primes

def count_sketch(data, w, d):
	primes = get_primes(1000, 3000)
	
	prime_sets = set()
	while len(prime_sets) < d:
		a, b = sample(primes, 2)
		if (a,b) not in prime_sets:
			prime_sets.add((a,b))
	ab_h = list(prime_sets)
	
	prime_sets = set()
	while len(prime_sets) < d:
		a, b = sample(primes, 2)
		if (a,b) not in prime_sets:
			prime_sets.add((a,b))
	ab_g = list(prime_sets)
	
	C = [[0]*w]*d
	g, h = [0]*d, [0]*d
	p = len(data)
	for item in data:
		for j in xrange(d):
			a, b = ab_g[j]
			col = universal_hash(item, a, b, p, w)
			a, b = ab_h[j]
			sig = plusminus1_hash(item, a, b, p, w)
			print item, j, sig
			C[j][col] += sig
	return C, ab_g, ab_h

def query_count_sketch(item, sketch, abhs, abgs):
	counts = []
	for j in xrange(len(sketch)):
		a, b = abhs[j]
		col = universal_hash(item, a, b, p, w)
			a, b = ab_h[j]
			sig = plusminus1_hash(item, a, b, p, w)
			print item, j, sig
			C[j][col] += sig

def test():
	data = [1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,3,3]
	#print frequent(data, 2)
	#print lossy_counting(data, 2)
	#print space_saving(data, 2)
	for row, _, _ in count_sketch(data, 3, 2):
		print row


test()

